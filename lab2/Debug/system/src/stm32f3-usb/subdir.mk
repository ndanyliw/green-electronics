################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../system/src/stm32f3-usb/usb_core.c \
../system/src/stm32f3-usb/usb_init.c \
../system/src/stm32f3-usb/usb_int.c \
../system/src/stm32f3-usb/usb_mem.c \
../system/src/stm32f3-usb/usb_regs.c \
../system/src/stm32f3-usb/usb_sil.c 

OBJS += \
./system/src/stm32f3-usb/usb_core.o \
./system/src/stm32f3-usb/usb_init.o \
./system/src/stm32f3-usb/usb_int.o \
./system/src/stm32f3-usb/usb_mem.o \
./system/src/stm32f3-usb/usb_regs.o \
./system/src/stm32f3-usb/usb_sil.o 

C_DEPS += \
./system/src/stm32f3-usb/usb_core.d \
./system/src/stm32f3-usb/usb_init.d \
./system/src/stm32f3-usb/usb_int.d \
./system/src/stm32f3-usb/usb_mem.d \
./system/src/stm32f3-usb/usb_regs.d \
./system/src/stm32f3-usb/usb_sil.d 


# Each subdirectory must supply rules for building sources it contributes
system/src/stm32f3-usb/%.o: ../system/src/stm32f3-usb/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -ffreestanding -Wunused -Wuninitialized -Wall -Wextra -Wmissing-declarations -Wconversion -Wpointer-arith -Wpadded -Wshadow -Wlogical-op -Waggregate-return -Wfloat-equal  -g3 -DDEBUG -DUSE_FULL_ASSERT -DTRACE -DOS_USE_TRACE_ITM -DSTM32F30X -DUSE_STDPERIPH_DRIVER -DHSE_VALUE=8000000 -I"../include" -I"../system/include" -I"../system/include/cmsis" -I"../system/include/stm32f3-stdperiph" -I../system/include/stm32f3-usb -I../bsp/inc -I../ee152_lib/inc -std=gnu11 -Wmissing-prototypes -Wstrict-prototypes -Wbad-function-cast -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


