#pragma once

#define TASK_POOL_SIZE 5
typedef struct task_s
{
	void (*h)(uint32_t);
	uint32_t param;
	struct task_s *next;
} task_t;

void task_init();
void task_poll();
void task_add(void (*handler)(uint32_t));