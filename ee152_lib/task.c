#include "task.h"
task_t *free_list;
task_t *full_list_head;
task_t *full_list_tail;
task_t pool[TASK_POOL_SIZE];
void task_init()
{
	free_list = &pool[0];
	full_list_head=0;
	full_list_tail=0;
	for (int i = 0; i < TASK_POOL_SIZE - 1; i++)
	{
		pool[i].next = &pool[i + 1];
	}
	pool[TASK_POOL_SIZE-1]=0;
}
void task_poll()
{
	if (!full_list_head)
	{
		return;
	}
	__disable_irq();

	task_t *task = full_list_head;
	full_list_head = full_list_head->next;
	if (!full_list_head)
	{
		full_list_head = 0;
	}

	void (*handler)(uint32_t) = task->handler;
	uint32_t param = task->param;
	task->next = free_list;
	free_list = task;

	__enable_irq();

	// Call the handler.
	handler(param);

	return;
}
void task_add(void (*handler)(uint32_t))
{
	assert(free_list!=0);
	task_t *f = free_list;
	free_list = free_list->next;

	f->h = handler;

	f->next = 0;
	if (full_list_tail)
	{
		full_list_tail->next = task;
	} else {
		// This is the only pending task.
		full_list_head = task;
	}
	full_list_tail = task;
}