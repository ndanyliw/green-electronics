#include "arm_math.h"
//This is an example of a FIR filter using the CMSIS arm dsp library.
//instances of q15 can be replaced with q7 or q31 for 8 and 32 bit
//data types respectively




//the arm_fir_q15() function can be replaced with arm_fir_fast_q15()
//in certain cases for better performance. The fast function uses a
//smaller accumulator and thus the programmer is responsible for
//ensuring that the filter does not overflow, by scaling the input
//by <= log2(numTaps) bits

//Numtaps must be >=4 and even. Round up and set the last tap to 0
//if an odd value is required.
#define FIR_NUM_TAPS 50

//blocksize is the number of samples to process per call
//Presumably, there are performance benefits for having a >1
//blocksize. This sample uses 4 to demonstrate behavior.
#define FIR_BLOCKSIZE 4

arm_fir_instance_q15 fir_struct;

//q15_t is a typedef for int16_t

//NOTE: coeffs must be BACKWARDS! coeffs[0] corresponds to the last
//tap.
//Coeffs can easily be generated from this tool 
//(http://t-filter.appspot.com/fir/index.html)
//though you must still reverse the coefficient order
static const q15_t coeffs[FIR_NUM_TAPS];
q15_t state[FIR_NUM_TAPS];

void fir_example()
{
	//Initialize FIR filter
	assert(arm_fir_init_q15(&fir_struct,&coeffs,&state,FIR_BLOCKSIZE)
		==ARM_MATH_SUCCESS);


	while(1)
	{
		//"get" input samples
		q15_t input_samples[FIR_BLOCKSIZE];

		//allocate space for output
		q15_t output_samples[FIR_BLOCKSIZE];

		//GO!
		arm_fir_q15(&fir_struct,&input_samples,&output_samples,
			FIR_BLOCKSIZE);

		//do something with output_samples now

	}



}