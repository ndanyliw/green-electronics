//
// This file is part of the GNU ARM Eclipse distribution.
// Copyright (c) 2014 Liviu Ionescu.
//

// ----------------------------------------------------------------------------

#include <stdio.h>
#include "diag/Trace.h"
// Includes for discovery board
#include "stm32f3_discovery.h"
#include "stm32f30x_exti.h"
// Include sensors
//#include "stm32f3_discovery_l3gd20.h"
//#include "stm32f3_discovery_lsm303dlhc.h"
// Include EE152 Libraries
#include "HD44780_F3.h"
#include "ee152_adc.h"
#include "ee152_pwm.h"
//#include "usart.h"

// ----------------------------------------------------------------------------
//
// Standalone STM32F3 empty sample (trace via ITM).
//
// Trace support is enabled by adding the TRACE macro definition.
// By default the trace messages are forwarded to the ITM output,
// but can be rerouted to any device or completely suppressed, by
// changing the definitions required in system/src/diag/trace_impl.c
// (currently OS_USE_TRACE_ITM, OS_USE_TRACE_SEMIHOSTING_DEBUG/_STDOUT).
//

// ----- main() ---------------------------------------------------------------

// Sample pragmas to cope with warnings. Please note the related line at
// the end of this function, used to pop the compiler diagnostics status.
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#pragma GCC diagnostic ignored "-Wmissing-declarations"
#pragma GCC diagnostic ignored "-Wreturn-type"


__IO uint32_t TimingDelay;

void float_to_string(char *buf, float val, char unit) {
  int a = (int)(val * 1000.0);
  int b = (int)(val>=0?a:-a);
  int v = (int)val;
  if(v==0 && val<0)
    {
    buf[0]='-';
    buf++;
    }
  sprintf(buf, "%d.%03d%c    ", v, b%1000,unit);
}


#define ADC1_OFFSET_VAL 0 //ground referenced, no negative range
#define ADC2_OFFSET_VAL 0 //ground referenced, no negative range

//Used as a resistor divider
#define VOLTAGE_DIVIDER_HIGH_RESISTOR 33000.0
#define VOLTAGE_DIVIDER_LOW_RESISTOR 1000.0
//Used in a non-inverting amplifier
#define CURRENT_DIVIDER_HIGH_RESISTOR 220000.0
#define CURRENT_DIVIDER_LOW_RESISTOR 15000.0


#define CURRENT_GAIN (((float)CURRENT_DIVIDER_HIGH_RESISTOR+(float)CURRENT_DIVIDER_LOW_RESISTOR)/(float)CURRENT_DIVIDER_LOW_RESISTOR)
#define VOLTAGE_GAIN (float)VOLTAGE_DIVIDER_LOW_RESISTOR/((float)VOLTAGE_DIVIDER_LOW_RESISTOR+(float)VOLTAGE_DIVIDER_HIGH_RESISTOR)

#define DIVS_PER_VOLT (4096/3.0)

//#define VOLTAGE_PER_DIV (1/(VOLTAGE_GAIN*DIVS_PER_VOLT))
//#define CURRENT_PER_DIV ((CURRENT_GAIN/DIVS_PER_VOLT)*100)
#define CURRENT_PER_DIV (.02)
#define VOLTAGE_PER_DIV (.0122)

#define PWM_FREQ 100000
#define CYCLES_PER_CONTROL_ITERATION 40

#define MAX_DUTY_CYCLE 0.9
//globals
volatile uint32_t global_time;
volatile float voltage;
volatile float current;
volatile float power;
volatile float duty;
volatile float direction;
volatile float old_power;
volatile int climbs_in_a_row;

void chan1_callback(uint32_t data);
//void EXTI0_Config(void);


int main(int argc, char* argv[])
{
  // At this stage the system clock should have already been configured
  // at high speed.

	  voltage = 0;
	  current = 0;
	  power   = 0;
	  old_power=0;
	  climbs_in_a_row=0;
	  direction=.02;
	  duty=.2;
  /* Setup SysTick Timer for 1 µsec interrupts  */
  //if (SysTick_Config(SystemCoreClock / 1000)) {
//    printf("Error with setting SysTick Timer\n");
  //}

  //HD44780_Init();
  //HD44780_GotoXY(0, 0);


  //initialize ADCs
  adc_init();

  //enable channel 1
  adc_enable_channel(1);
  adc_callback(1, chan1_callback);

  adc_set_fs(9000);

  adc_start();


  //initialize pwm module
  init_pwm();

  set_pwm_freq(PWM_FREQ);
  set_pwm(1, duty);
  enable_pwm_chan(1);


  for(;;) {

    //set_pwm(1, duty);

  }
}

//void chan1_callback(uint32_t data) {
////  voltage = ((float)((int)(data & 0xffff)-ADC1_OFFSET_VAL))*VOLTAGE_PER_DIV;
//  current = ((float)((int)(data >> 16)-ADC2_OFFSET_VAL))*CURRENT_PER_DIV;
//  //power = voltage*current;
//
////  printf("%d\n", power);
//}

volatile float old_duty=0;
void calculate_new_pwm()
{
	old_duty=duty;
	if(power>old_power)
	{
		climbs_in_a_row++;
		duty+=climbs_in_a_row*direction;
	}else
	{
		climbs_in_a_row=0;
		direction=-direction;
		duty+=direction;
	}
	old_power=power;
}
volatile int avg_cnt=0;
#define AVGNUM 80
void chan1_callback(uint32_t data) {
	if(avg_cnt<AVGNUM)
	{
		avg_cnt++;
  voltage += ((float)((int)(data & 0xffff)-ADC1_OFFSET_VAL))*VOLTAGE_PER_DIV/AVGNUM;
  current += ((float)((int)(data >> 16)-ADC2_OFFSET_VAL))*CURRENT_PER_DIV/AVGNUM;
	}else{
		avg_cnt=0;
  power = voltage*current;
  calculate_new_pwm();
  if(duty>=get_max_pwm_int()*MAX_DUTY_CYCLE)
  {
	  duty=get_max_pwm_int()*MAX_DUTY_CYCLE;
  }
  if(duty<0)
  {
	  duty=0;
  }
  //set_pwm(1,.5);
  set_pwm(1,1-duty);
  voltage=0;
  current=0;
	}
}
extern "C" {
  /*void EXTI0_IRQHandler(void)
  {
    if ((EXTI_GetITStatus(EXTI_Line0) == SET)&&(GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_0)))
    {
      dt_rot = global_time - last_rot_time;
      if (dt_rot >= 3) {
        rot_count++;
        last_rot_time = global_time;
      }
//      float next_speed = 1/((float)dt_rot)*3.14159*1000.0;
//      if((next_speed < 1000)) {
//        last_speed = curr_speed;
//        curr_speed = next_speed;
//        last_rot_time = global_time;
//        rot_count += 1;
//      }

      // Clear the EXTI line 0 pending bit
      EXTI_ClearITPendingBit(EXTI_Line0);
    }
    // Clear the EXTI line 0 pending bit
    EXTI_ClearITPendingBit(EXTI_Line0);
  }*/
}

void SysTick_Handler(void)
{
  global_time++;
  TimingDelay--;
}

void _delay_us(__IO uint32_t nTime)
{
  TimingDelay = nTime/1024;

  while(TimingDelay != 0);
}

void _delay_ms(__IO uint32_t nTime)
{
  TimingDelay = nTime;

  while(TimingDelay != 0);
}


#pragma GCC diagnostic pop

// ----------------------------------------------------------------------------

//#include "stm32f3_discovery.h"
//#include "cortexm/ExceptionHandlers.h"
//
//static void ADC_Config(void);
//static void DAC_Config(void);
//
//int main(void){
//  DAC_Config();
//  ADC_Config();
//  for(;;);
//}
//
//static void DAC_Config(void){
//  DAC_InitTypeDef    DAC_InitStructure;
//  GPIO_InitTypeDef   GPIO_InitStructure;
//
//  /* Enable GPIOA clock */
//  RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);
//
//  /* Configure PA.04 (DAC1_OUT1) in analog mode -------------------------*/
//  GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_AN;
//  GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_NOPULL;
//  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_4;
//  GPIO_Init(GPIOA, &GPIO_InitStructure);
//
//  /* Enable DAC clock */
//  RCC_APB1PeriphClockCmd(RCC_APB1Periph_DAC, ENABLE);
//
//  /* DAC1 channel1 Configuration */
//  DAC_InitStructure.DAC_Trigger = DAC_Trigger_None;
//  DAC_InitStructure.DAC_WaveGeneration = DAC_WaveGeneration_None;
//  DAC_InitStructure.DAC_LFSRUnmask_TriangleAmplitude = DAC_LFSRUnmask_Bits2_0;
//  DAC_InitStructure.DAC_OutputBuffer = DAC_OutputBuffer_Disable;
//  DAC_Init(DAC_Channel_1, &DAC_InitStructure);
//
//  /* Enable DAC1 Channel1 */
//  DAC_Cmd(DAC_Channel_1, ENABLE);
//}
//
//static void ADC_Config(void){
//  ADC_InitTypeDef       ADC_InitStructure;
//  ADC_CommonInitTypeDef ADC_CommonInitStructure;
//  GPIO_InitTypeDef   GPIO_InitStructure;
//  NVIC_InitTypeDef   NVIC_InitStructure;
//  __IO uint16_t calibration_value = 0;
//
//  /* GPIOC Periph clock enable */
//  RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOC, ENABLE);
//
//  /* Configure the ADC clock */
//  RCC_ADCCLKConfig(RCC_ADC12PLLCLK_Div2);
//
//  /* ADC1 Periph clock enable */
//  RCC_AHBPeriphClockCmd(RCC_AHBPeriph_ADC12, ENABLE);
//
//  /* Setup SysTick Timer for 1 µsec interrupts  */
//  if(SysTick_Config(SystemCoreClock / 1000000)){
//    for(;;);
//  }
//
//  /* ADC Channel configuration */
//  /* Configure ADC Channel7 (PC1) as analog input */
//  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_1 ;
//  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AN;
//  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL ;
//  GPIO_Init(GPIOC, &GPIO_InitStructure);
//
//  ADC_StructInit(&ADC_InitStructure);
//
//  /* Calibration procedure */
//  ADC_VoltageRegulatorCmd(ADC1, ENABLE);
//
//  ADC_SelectCalibrationMode(ADC1, ADC_CalibrationMode_Single);
//  ADC_StartCalibration(ADC1);
//
//  while(ADC_GetCalibrationStatus(ADC1) != RESET );
//  calibration_value = ADC_GetCalibrationValue(ADC1);
//
//  /* Configure the ADC1 in continuous mode */
//  ADC_CommonInitStructure.ADC_Mode = ADC_Mode_Independent;
//  ADC_CommonInitStructure.ADC_Clock = ADC_Clock_AsynClkMode;
//  ADC_CommonInitStructure.ADC_DMAAccessMode = ADC_DMAAccessMode_Disabled;
//  ADC_CommonInitStructure.ADC_DMAMode = ADC_DMAMode_OneShot;
//  ADC_CommonInitStructure.ADC_TwoSamplingDelay = 0;
//
//  ADC_CommonInit(ADC1, &ADC_CommonInitStructure);
//
//  ADC_InitStructure.ADC_ContinuousConvMode = ADC_ContinuousConvMode_Enable;
//  ADC_InitStructure.ADC_Resolution = ADC_Resolution_12b;
//  ADC_InitStructure.ADC_ExternalTrigConvEvent = ADC_ExternalTrigConvEvent_0;
//  ADC_InitStructure.ADC_ExternalTrigEventEdge = ADC_ExternalTrigInjecConvEvent_0;
//  ADC_InitStructure.ADC_DataAlign = ADC_DataAlign_Right;
//  ADC_InitStructure.ADC_OverrunMode = ADC_OverrunMode_Disable;
//  ADC_InitStructure.ADC_AutoInjMode = ADC_AutoInjec_Disable;
//  ADC_InitStructure.ADC_NbrOfRegChannel = 1;
//  ADC_Init(ADC1, &ADC_InitStructure);
//
//  /* ADC1 regular channel7 configuration */
//  ADC_RegularChannelConfig(ADC1, ADC_Channel_7, 1, ADC_SampleTime_7Cycles5);
//
//  /* Enable End Of Conversion interrupt */
//  ADC_ITConfig(ADC1, ADC_IT_EOC, ENABLE);
//
//  /* Configure and enable ADC1 interrupt */
//  NVIC_InitStructure.NVIC_IRQChannel = ADC1_2_IRQn;
//  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
//  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
//  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
//  NVIC_Init(&NVIC_InitStructure);
//
//  /* Enable ADC1 */
//  ADC_Cmd(ADC1, ENABLE);
//
//  /* wait for ADRDY */
//  while(!ADC_GetFlagStatus(ADC1, ADC_FLAG_RDY));
//
//  /* ADC1 Start Conversion */
//  ADC_StartConversion(ADC1);
//  /* doesn't return ? */
//}
//
//
//uint32_t ADCVal = 0;
//void SysTick_Handler(void){}
//
///**
//   This function handles ADC1 interrupt requests
//  */
//void ADC1_2_IRQHandler(void){
//  if(ADC_GetITStatus(ADC1, ADC_IT_EOC) != RESET)
//  {
//    /* Get converted value */
//    ADCVal = ADC_GetConversionValue(ADC1);
//
//    /* Output converted value on DAC1_OUT1 */
//    DAC_SetChannel1Data(DAC_Align_12b_R, ADCVal);
//
//    /* Clear EOC Flag */
//    ADC_ClearFlag(ADC1, ADC_FLAG_EOC);
//  }
//}
