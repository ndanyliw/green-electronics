% adjust off time for QRB converter

cum_sum = meas + cum_sum - mem(N);
avg_meas = cum_sum/N;

mem(2:end) = mem(1:N-1);
mem(1) = meas;

error = v_ref/v_s - avg_meas/v_s;

cum_error = error*delta_t + cum_error;
d_error = (error - old_error)/delta_t;

% off_time_us = -kp*error - ki*cum_error - kd*d_error;
% off_time = off_time_us*1e-6;

fn = 1/(2*pi)*1/sqrt(l_r*c_r);
zn = sqrt(l_r/c_r);
r = r_l/zn;

x = kp*error + ki*cum_error + kd*d_error + k_ff*v_ref/v_s;

if(x <= 0)
    x = .01;
elseif (x >= max_duty)
    x = max_duty;
end

f_switch = 2*pi*fn*x*(x/(2*r)+pi+asin(x/r)+r/x*(1+sqrt(1-(x/r)^2)))^-1;

off_time = 1/f_switch - (t2-t0);

old_error = error;

if off_time < min_off_time
    off_time = min_off_time;
elseif off_time > max_off_time
    off_time = max_off_time;
end