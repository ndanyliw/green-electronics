% simulate one variable width PWM cycle of a QR ZCS buck converter

% by convention cycle starts with high switch turning on
t0 = t ;
s_h = 1 ; 
while((v_x < v_s) || (i_r > 0))
  qrb_step ;
end
t1 = t ;

% wait until c_r is drained
s_h = 0 ;
while(v_x > 0)
  qrb_step ;
end 
t2 = t ;

% then wait dead time
off_steps = off_time/dt ;
for i5 = 1:off_steps
  qrb_step ;
end
t3 = t ;

t_c = t3-t0 ;

% log cycle variables
tcy(i3) = t_c ;
ty(i3) = t ;
i3 = i3+1 ;


