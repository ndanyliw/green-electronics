% simulate one time step of QR QCS buck converter
% state variables are v_x, i_r, i_l, v_c
% switch is s_h
% each update is based on last cycle's values

v_l = v_x - v_c ; % inductor voltage

% update drain voltage based on switch positions
if(s_h) 
  v_r = v_s-v_x ;
else
  v_r = 0 ;
  i_r = 0 ;
end

% integrate
v_c = v_c + (i_l-v_c/r_l)*dt/c ;
v_x = v_x + (i_r-i_l)*dt/c_r ;
if(v_x < 0) 
  v_x = 0  ; % diode D1
end

i_l = i_l + v_l*dt/l ;
i_r = i_r + v_r*dt/l_r ;

% advance time and log variables for plots
t = t + dt ;
qrb_log_step ;


