% initialization code

% component values
l = 44E-6 ;
c = 100E-6 ;
c_r = 5E-9 ;
l_r = 4.7e-6;
r_l = 22 ;

% constants
dt = 1E-9 ;

% supplies and thresholds
v_s = 450 ;

% initial values
v_c = 180 ;
v_ref = 180 ;
v_x = 0 ;
i_l = (v_c/r_l)-1.5 ;
i_r = 0 ;
off_time = 1e-6 ;

% time
t = 0 ;
i1 = 1 ;
i3 = 1 ;
t_min = 0 ;

% controller
%number of cycles to average for controller
N = 1;

%base parameters
kp_base = 300;
ki_base = 1000000;
kd_base = .001;
k_ff_base = 1;
rl_base = 22;

%calculate parameters from base parameters
kp = kp_base/(r_l/rl_base)^2;
ki = ki_base/(r_l/rl_base)^2;
kd = kd_base/(r_l/rl_base)^2;
k_ff = k_ff_base;

% %22 load
% kp = 300;
% ki = 1000000;
% kd = .001;
% k_ff = 1;

%220 load
% kp = 3;
% ki = 10000;
% kd = .0001;
% k_ff = 1;

%2200 load
% kp = .03;
% ki = 100;
% kd = .000001;
% k_ff = 1;

cum_sum = N*v_c;
old_error = 0;
cum_error = 0;
mem = ones(N,1)*v_c;

max_off_time = 200e-6;
max_duty = .9;
