clear all;
qrb_start ;
num_cycles = 50;
off_time_mem = zeros(num_cycles, 1);
duty_mem = zeros(num_cycles, 1);
for i=1:num_cycles
    off_time_mem(i) = off_time;
    qrb_cycle ;
    meas = v_c;
    delta_t = t_c;
    min_off_time = (t2-t0)/max_duty-(t2-t0);
    qrb_controller;
    error_mem(i) = error;
    duty_mem(i) = x;
end

figure(1) ;
xmax = t*1e6 ;
qrb_plot ;

figure(2) ;
xmax = 2 ;
qrb_plot ;



