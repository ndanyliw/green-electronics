#include <avr/io.h>
#include <avr/interrupt.h>

#include "serial.h"

uint8_t spi_xfer(uint8_t tx)
{
	SPDR = tx;
	loop_until_bit_is_set(SPSR, SPIF);
	uint8_t rx = SPDR;
	return rx;
}

int main()
{
	serial_init();
	sei();
	fprintf(&serial, "Init\n");

	PORTD = 0x80;
	DDRD = 0x80;

	PORTB = 0x10;
	DDRB = 0xb0;

	SPCR = (1 << SPE) | (1 << MSTR) | 1;
	SPSR = (1 << SPI2X);

	while (1)
	{
		PORTD &= ~0x80;
		uint8_t a = spi_xfer(0);
		uint8_t b = spi_xfer(0);
		uint8_t c = spi_xfer(0);
		uint8_t d = spi_xfer(0);
		PORTD |= 0x80;
		fprintf(&serial, "Got %02x %02x %02x %02x\n", a, b, c, d);
	}

	return 0;
}
