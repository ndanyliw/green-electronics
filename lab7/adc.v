// This module drives two LTC1407 ADCs.

module adc (
	input clk,
	input start,
	output running,
	output reg sck = 0,
	output reg conv = 0,
	input data01,
	input data23,
	output reg [11:0] result0 = 0,
	output reg [11:0] result1 = 0,
	output reg [11:0] result2 = 0,
	output reg [11:0] result3 = 0
);

// References:
// [1] LTC1407 datasheet, http://cds.linear.com/docs/Datasheet/14071fb.pdf

// SCK idles low.

// State is measured in bit times (SCK periods).
// State 0 is idle, so the state numbers match the timing diagrams on page 9 of the datasheet [1].
reg [5:0] state = 0;

assign running = (state != 0);

// A shift register for each ADC's raw data
reg [27:0] sr01 = 0;
reg [27:0] sr23 = 0;

// Synchronized copies of input lines
reg [1:0] data01_s;
reg [1:0] data23_s;

// The ADC maximum clock frequency is 51MHz, but running at 50MHz causes incorrect data to be received
// due to clock skew due to the long VHDCI cable.
// Just divide the clock again, for a SCK frequency of clk/4 (25MHz).
reg clk_div = 0;

reg sck_d1 = 0;

always @(posedge clk) begin
	// Synchronize inputs
	data01_s <= {data01_s[0], data01};
	data23_s <= {data23_s[0], data23};

	clk_div <= ~clk_div;

	sck_d1 <= sck;

	// State machine
	if (!running && start) begin
		// Start the state machine when requested
		state <= 1;
		conv <= 1;
	end else if (running) begin
		// Drive outputs
		if (clk_div == 0) begin
			// Toggle SCK
			sck <= ~sck;

			// Only keep CONV high for one SCK cycle.
			// There is only a minimum high time.
			if (sck == 1) begin
				conv <= 0;
			end
		end

		// Handle inputs, which are delayed two master clock cycles
		if (sck_d1 == 0 && sck == 1) begin
			// Advance the state machine
			if (state == 34) begin
				// This transfer has finished and enough time has elapsed to start a new conversion.
				if (start) begin
					// Another conversion is requested, so start it now.
					// This allows the clock to keep running at a constant frequency.
					state <= 1;
					conv <= 1;
				end else begin
					// Idle
					state <= 0;
				end
			end else begin
				state <= state + 6'd1;
			end

			// The ADC shifts its output (data01/data23) shortly after the rising edge of SCK,
			// with a minimum delay of 8ns which is intended to allow sampling on the rising edge.
			//
			// data01_s and data23_s are the ADC data on the last master clock cycle, which was
			// the rising edge of SCK.  Shift them into the shift registers now, since they may
			// be invalid on the next cycle.

			sr01 <= {sr01[26:0], data01_s[1]};
			sr23 <= {sr23[26:0], data23_s[1]};

			if (state == 32) begin
				// The last bit of channel 1's result was shifted out of the ADC during state 31.
				// It was shifted in to our shift registers during state 32, which has just ended.
				//
				// This is the end of data in one transfer, so capture the results.
				result0 <= sr01[27:16];
				result1 <= sr01[11:0];

				result2 <= sr23[27:16];
				result3 <= sr23[11:0];
			end
		end
	end
end

endmodule
