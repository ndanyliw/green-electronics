`include "dac.v"
`include "adc.v"
`include "spi.v"
`include "pwm.v"
`include "slow_controller.v"

module lab6
(
	input clk_100MHz,

	output dac0_data,
	output dac1_data,
	output dac_sck,
	output dac_ncs,

	input adc01_data,
	input adc23_data,
	output adc_conv,

	// The ADC SCK is driven differentially in an attempt to make the signal cleaner by the time
	// it reaches the interface board.
	// I'm not sure if this actually helps.
	output adc_sck_p, adc_sck_n,

	output reg [7:0] led = 0,
	input [5:0] button,
	input [7:0] switch,

	// These are not an array so their directions can be changed independently.
	output gpio0,
	input gpio1,
	input gpio2,
	input gpio3,

	// Comparator outputs
	input comp0_out, comp1_out,

	// SPI interface to microcontroller
	output mcu_miso,
	input mcu_mosi, mcu_sck, mcu_ncs
);

// The clock frequency could be adjusted here with a DCM.
wire clk = clk_100MHz;

////////////////
// SPI interface
//
// The first byte sent to the MCU is always Logic_Version.

localparam Logic_Version = 8'ha5;

wire [7:0] rx_data;
reg [7:0] tx_data = 0;
wire spi_selected, spi_transfer_strobe;
spi spi(
	.clk(clk),
	.async_sck(mcu_sck),
	.async_mosi(mcu_mosi),
	.async_ncs(mcu_ncs),
	.miso(mcu_miso),
	.selected(spi_selected),
	.transfer_strobe(spi_transfer_strobe),
    .rx_data(rx_data),
    .tx_data(tx_data)
	);

// The number of the byte that was just transferred,
// whenever spi_transfer_strobe goes high.
reg [3:0] spi_count = 0;

always @(posedge clk) begin
	if (!spi_selected) begin
		// Set up the first byte to be transmitted
		tx_data <= Logic_Version;
		spi_count <= 0;
	end else if (spi_transfer_strobe) begin
		spi_count <= spi_count + 4'd1;
		//FIXME - Real data
        tx_data <= spi_count ^ 8'hc9;// ^ {7'b0, term};
    end
end

// Maximum ADC SCK frequency is 51MHz, but limited to 25MHz because of the long cable.
// Maximum DAC SCK frequency is 20MHz.

wire dac_start = 1;
wire dac_running;
reg dac_running_d = 0;

reg [11:0] dac0_value = 0;
reg [11:0] dac1_value = 0;

// DAC configuration:
//   Bit 2: 1=Buffer enabled
//   Bit 1: 1=Gain of 1
//   Bit 0: 1=DAC operational (not shutdown)
wire [2:0] dac0_config = 3'b111;
wire [2:0] dac1_config = 3'b111;

dac dac(
	.clk(clk),
	.start(dac_start),
	.word0({1'b0, dac0_config, dac0_value}),
	.word1({1'b0, dac1_config, dac1_value}),
	.data0(dac0_data),
	.data1(dac1_data),
	.sck(dac_sck),
	.ncs(dac_ncs),
	.running(dac_running)
);

// DAC demo.
always @(posedge clk) begin
	dac_running_d <= dac_running;

	if (dac_running_d == 1 && dac_running == 0) begin
		// DAC just finished
		dac0_value <= dac0_value + 12'd1;
		dac1_value <= dac1_value + 12'd1;
	end
end

wire adc_start = 1;
wire adc_running;
wire [11:0] adc_result_0, adc_result_1, adc_result_2, adc_result_3;

wire adc_sck;
assign adc_sck_p = adc_sck;
assign adc_sck_n = ~adc_sck;

adc adc(
	.clk(clk),
	.start(adc_start),
	.running(adc_running),
	.sck(adc_sck),
	.conv(adc_conv),
	.data01(adc01_data),
	.data23(adc23_data),
	.result0(adc_result_0),
	.result1(adc_result_1),
	.result2(adc_result_2),
	.result3(adc_result_3)
);

// Switch and LED demo: show ADC results.
wire [1:0] adc_sel = switch[1:0];
wire [11:0] adc_display =
	(adc_sel == 0) ? adc_result_0 :
	(adc_sel == 1) ? adc_result_1 :
	(adc_sel == 2) ? adc_result_2 :
	                 adc_result_3;

always @(posedge clk) begin
	if (switch[2]) begin
		led <= adc_display[7:0];
	end else begin
		led <= adc_display[11:4];
	end
end

wire [8:0] pwm_phase;
wire [8:0] pwm_level;

// Generate a start signal for the controller at the beginning of every PWM cycle.
// 
// This is the same cycle that pwm_level is latched by the PWM module,
// so this is the earliest that a controller can start calculating a new value.
reg start = 0;
always @(posedge clk) begin
	start <= (pwm_phase == 9'h1ff);
end

slow_controller controller(
	.clk(clk),
	.start(start),
	.voltage(adc_result_3),
	.pwm_level(pwm_level),
	.sw(switch[7:5])
	);

pwm pwm(
	.clk(clk),
	.level(pwm_level),
	.phase(pwm_phase),
	.pwm(gpio0)
);

endmodule
