module pwm (
	input clk,

	// The duty cycle, from 0 to 511.
	// This takes effect during the cycle that phase becomes 0.
	// It only need to be valid when (phase == 9'h1ff).
	input [8:0] level,

	// The phase of the PWM signal, from 0 to 511.
	// This may be used to synchronize other activites with the PWM period.
	output reg [8:0] phase = 0,

	// The PWM output.  Connect this to an output pin.
	output reg pwm = 0
);

// level is double buffered.
// effective_level is the one that is actually used.
reg [8:0] effective_level = 0;

always @(posedge clk) begin
	phase <= phase + 9'd1;

	// Use the new level at the beginning of each PWM period.
	if (phase == 9'h1ff) begin
		effective_level <= level;
	end

	pwm <= phase < effective_level;
end

endmodule
