module test_controller ();
reg clk,s;
reg [11:0] v;
wire [8:0] p;
slow_controller uit(.clk(clk),.start(s),.voltage(v),.pwm_level(p));
initial begin
	$dumpvars;
	clk=0;
	forever begin
		#5clk=!clk;
	end
end

initial begin
	v=0;
	#5
	v=1500;
	s=1;
	#10
	s=0;
	#50
	v=2000;
	s=1;
	#10
	s=0;
	#50
	v=1559;
	s=1;
	#10
	s=0;
	#50
	$finish;
end

endmodule
