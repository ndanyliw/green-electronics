// This module drives two MCP49x1 DAC's at the same time, with common SCK and /CS lines.
// The EE152 interface board uses MCP4911s.
// Set <word0> and <word1> to the words to be sent to the DACs (see the datasheet [1] page 24 for details)
// and drive <start> high until <running> goes high.
//
// <word0> and <word1> only need to be valid while <start> is high.
// <start> may be tied high to repeatedly send the current data.
//
// <clk> is expected to be <= 100MHz.
// All signals are synchronous to <clk>.
//
// References:
// [1] MCP49x1 datasheet, http://ww1.microchip.com/downloads/en/DeviceDoc/22248a.pdf

module dac (
	input clk,
	input start,
	input [15:0] word0, word1,
	output running,
	output data0, data1,
	output reg sck = 0,
	output reg ncs = 1
);

// Shift registers
reg [15:0] sr0 = 0;
reg [15:0] sr1 = 0;

assign data0 = sr0[15];
assign data1 = sr1[15];

// SCK clock divider.
// sck_edge goes high for one clk cycle every time sck can toggle.
// clk can only be divided by mutiples of 2, and sck has a maximum frequency of 20MHz.
reg [1:0] dac_div = 0;
wire sck_edge = (dac_div == 0);
always @(posedge clk) begin
	if (dac_div == 3) begin
		dac_div <= 0;
	end else begin
		dac_div <= dac_div + 3'd1;
	end
end

// DAC SPI state machine:
// State 0 is idle.
// States 1-32 are shifting data, with the data changing after each odd state.
// States 33-34 are /CS high and SCK idle
//
// After state 32, we shift data one last time to make the data line zero,
// SCK falls so that it idles low, and /CS goes high.
reg [5:0] state = 0;

assign running = (state != 0);
always @(posedge clk) begin
	// Start a new transfer when requested.
	if (start && !running && sck_edge) begin
		sr0 <= word0;
		sr1 <= word1;
		state <= 1;
		ncs <= 0;
	end

	if (running && sck_edge) begin
		// Toggle SCK
		if (state <= 32) begin
			sck <= ~sck;
		end

		// Next state
		state <= state + 6'd1;

		if (state == 32) begin
			ncs <= 1;
		end

		if (state == 34) begin
			state <= 0;
		end
	end

	// Shift the data while we're running and about to make a falling edge on SCK.
	// This shifts one last time on the last falling edge of SCK,
	// always driving a zero because all data has been shifted out.
	if (running && sck_edge && sck == 1) begin
		sr0 <= {sr0[14:0], 1'b0};
		sr1 <= {sr1[14:0], 1'b0};
	end
end

endmodule
