`timescale 1ns/1ns

`include "spi.v"

module main;
    reg clk = 0;
    reg ncs = 1, mosi = 0, sck = 0;
    wire miso;
    wire spi_selected, spi_transfer_strobe;
    wire [7:0] rx_data;
    reg [7:0] tx_data = 8'ha5;

    // DUT
    spi dut(
		.clk(clk),
		.async_sck(sck),
		.async_mosi(mosi),
		.async_ncs(ncs),
		.miso(miso),
        .selected(spi_selected),
		.transfer_strobe(spi_transfer_strobe),
        .rx_data(rx_data),
        .tx_data(tx_data)
    	);

    reg [7:0] sel_count = 0;
    always @(posedge clk) begin
        if (!spi_selected) begin
            tx_data <= sel_count;
        end else if (spi_transfer_strobe) begin
            tx_data <= sel_count; //rx_data ^ 8'hc9;
            sel_count <= sel_count + 8'd1;
        end
    end

    // Clock
    initial begin
        clk = 0;
        forever begin
            #5 clk = ~clk;
        end
    end
    
    task spi_on;
        begin
            ncs = 0; #2;
        end
    endtask
    
    task spi_off;
        begin
            #2 ncs = 1; #4;
        end
    endtask
    
    task spi;
        input [7:0] in;
       	integer i;
        
        begin
        	for (i = 7; i >= 0; i = i - 1) begin
				mosi = in[i]; #20 sck = 1; #20 sck = 0;
        	end
        end
    endtask
    
    initial begin
    #30 spi_on();
        spi(8'h22);
        spi(8'h00);
        spi(8'h00);
        spi(8'h00);
        spi(8'h00);
    #20 spi_off();
    end

    // Logging
    initial begin
    	$dumpfile("sim-spi.vcd");
        $dumpvars;
        $dumpall;
        #2000
        $finish;
    end
endmodule
