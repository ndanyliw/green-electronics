// This is a synchronous SPI slave interface for SPI mode 0.
// The maximum SCK frequency is clk/4, due to synchronization delays.

module spi (
	// Master clock
	input clk,

	// Asynchronous inputs from off-chip
	input async_sck, async_mosi, async_ncs,

	// Off-chip output
	output miso,

	// transfer_strobe goes high for one clk cycle after the last bit in each byte
	// is captured.  The data to be transmitted for the next byte must be present
	// on tx_data for the next clk cycle.  tx_data only needs to be valid for one
	// clk cycle after the one during which transfer_strobe is high.
	output reg transfer_strobe = 0,

	// High when this device is selected.
	// This is just a synchronized, active high CS.
	output selected,

	// Most recent byte received.
	output reg [7:0] rx_data = 0,

	// Next byte to be transmitted.
	// This need to be updated the cycle after transfer_strobe is high.
	input [7:0] tx_data
);

// Synchronize inputs.
reg sck = 0, mosi = 0, ncs = 0;
always @(posedge clk) begin
	sck <= async_sck;
	mosi <= async_mosi;
	ncs <= async_ncs;
end

assign selected = ~ncs;

// Data shift register.
reg [7:0] shift_register = 0;
reg mosi_capture = 0;

// Serial output.
// This directly drives a pin from the shift register.
assign miso = (async_ncs == 0) ? shift_register[7] : 1'bz;

reg [2:0] bit_count = 0;
wire last_bit = (bit_count == 7);

// Detect SCK edges.
// This uses SPI mode 0: capture on the rising edge, shift on the falling edge.
reg sck_d = 0;
always @(posedge clk) begin
	sck_d <= sck;
end
wire sck_capture_edge = (sck == 1) && (sck_d == 0);
wire sck_shift_edge = (sck == 0) && (sck_d == 1);

// Shift data and count bits.
always @(posedge clk) begin
	if (ncs == 1) begin
		// While /CS is inactive, keep the bit count and shift register reset.
		bit_count <= 0;
		shift_register <= tx_data;
	end else begin
		// /CS is active.

		// Capture incoming data on one SCK edge,
		// and shift it on another.
		//
		// Output data is wired to one end of the shift register,
		// so it changes as a result of shifting.

		if (sck_capture_edge) begin
			mosi_capture <= mosi;
			if (last_bit) begin
				rx_data <= {shift_register[6:0], mosi};
			end
		end

		if (sck_shift_edge) begin
			bit_count <= bit_count + 3'b1;
			if (last_bit) begin
				shift_register <= tx_data;
			end else begin
				shift_register <= {shift_register[6:0], mosi_capture};
			end
		end
	end

	// Pulse transfer_strobe as the last bit of a byte is captured.
	transfer_strobe <= sck_capture_edge && last_bit;
end

endmodule
