module slow_controller (
	input clk,
	input start,
	input [11:0] voltage,
	output reg [8:0] pwm_level = 0,
	input [2:0] sw
);
//div=47k/(200k+47k) diff on 2.5v ref with 12bit adc
wire signed [12:0] setpoint = 1434;
reg signed [12:0] error;
wire signed [12:0] Kp=256;//201790;
wire signed [12:0] Ki=2;
wire signed [12:0] Kd=50;//3363;
reg signed [26:0] Perror;
reg signed [26:0] Ierror;
reg signed [26:0] Derror;
reg signed [26:0] Derror_temp;
reg [28:0] pwm_full=0;
wire ovf= pwm_full[27:12]>400;
always @(*) begin
	if(ovf) begin
		pwm_level=400;
	end else if(pwm_full<0) begin
		pwm_level=0;
	end else begin
		pwm_level=pwm_full[20:12];
	end
end
reg [1:0] state=0;
always @(posedge clk) begin
	// Half way through each PWM cycle (as the old PWM level is used),
	// start calculating the value for the next cycle.
	// 
	// This gives a latency of one half PWM cycle plus the time it takes
	// to make the ADC measurements.
	// 
	// This should be more than enough time to do any calculations
	// you need for your controller.
	if(state==2) begin
		state<=0;
		
		Derror<=(Derror_temp)*Kd;
	end else if(state==1) begin
		Perror<=sw[2]?Kp*error:0;
		Ierror<=sw[1]?(Ierror+Ki*error):0;
		Derror_temp<=sw[0]?(Derror-error):0;
		state<=2;
	end else if (start) begin
		
		error<=setpoint-voltage;
		state<=1;
	end else begin
		state<=0;
		pwm_full<=Perror+Ierror+Derror;
		//pwm_full<=Perror;
	end
	/*if(start) begin
	if (voltage < setpoint) begin
			if (pwm_level < 400) begin
				pwm_level <= pwm_level + 9'd1;
			end
		end else if (voltage > setpoint) begin
			if (pwm_level > 0) begin
				pwm_level <= pwm_level - 9'd1;
			end
		end
	end*/
end

endmodule
