/**
 *****************************************************************************
 **
 **  File        : main.c
 **
 **  Abstract    : main function.
 **
 **  Functions   : main
 **
 **  Environment : Atollic TrueSTUDIO(R)
 **
 **  Distribution: The file is distributed �as is,� without any warranty
 **                of any kind.
 **
 **  (c)Copyright Atollic AB.
 **  You may use this file as-is or modify it according to the needs of your
 **  project. Distribution of this file (unmodified or modified) is not
 **  permitted. Atollic AB permit registered Atollic TrueSTUDIO(R) users the
 **  rights to distribute the assembled, compiled & linked contents of this
 **  file as part of an application binary file, provided that it is built
 **  using the Atollic TrueSTUDIO(R) toolchain.
 **
 *****************************************************************************
 */

/* Includes */
#include "stm32f30x.h"
#include "stm32f3_discovery.h"
#include "common.h"
#include "stm32f30x_rcc.h"
#include "HD44780_F3.h"
#include "usart.h"
/* Private typedef */

/* Private define  */
#define DELAY	(1000)

/* Private macro */

/* Private variables */
float a, b, c, d;

volatile float pwm_value;
#define PWM_DELTA (.02)
#define MAX_PWM 1
volatile int ms;
volatile int last_button;
volatile int button_cnt;
volatile int running;
void EXTI0_IRQHandler(void)
{
  //if ((EXTI_GetITStatus(USER_BUTTON_EXTI_LINE) == SET)&&(STM_EVAL_PBGetState(BUTTON_USER) != RESET))
  {
    unsigned int time=ms;
	if(time-last_button>50)//50ms
	{
		//is a valid press
		last_button=time;
		button_cnt++;
	}

    /* Clear the EXTI line 0 pending bit */

  }
  EXTI_ClearITPendingBit(USER_BUTTON_EXTI_LINE);
}
void set_duty_cycle(float duty)
{
	TIM_SetCompare1(TIM1 , (TIM1->ARR)*duty);
}
float get_duty_cycle_epsilon()
{
	float i=1;

	return i/(TIM1->ARR);
}
void ss_1()
{
	pwm_value+=PWM_DELTA;
	if(pwm_value>MAX_PWM)
	{
		pwm_value=MAX_PWM;
	}
	set_duty_cycle(pwm_value);

}
void ss_2()
{
	 pwm_value-=PWM_DELTA;
	if(pwm_value<0)
	{
		pwm_value=0;
	}
	set_duty_cycle(pwm_value);
}
void init_pwm_pinmux()
{
	GPIO_InitTypeDef GPIO_InitStructure;
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOE, ENABLE);
	GPIO_InitStructure.GPIO_Pin = LED3_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF; // Use the alternative pin functions
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz; // GPIO speed - has nothing to do with the timer timing
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP; // Push-pull
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP; // Setup pull-up resistors
	GPIO_Init(GPIOE, &GPIO_InitStructure);
	GPIO_PinAFConfig(GPIOE, GPIO_PinSource9, GPIO_AF_2); // TIM1_CH1 -> LED3
}

int init_pwm(int pwm_freq)
{
	// Calculates the timing. This is common for all channels
	int tim_freq = 144e6; // in Hz (144MHz) Base frequency of the pwm timer
	int prescaler = 0;

	// Calculate the period for a given pwm frequency
	int pwm_period = tim_freq/pwm_freq;

//  Enable the TIM1 peripherie
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM1 , ENABLE );

// Setup the timing and configure the TIM1 timer
	TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
	TIM_TimeBaseStructInit(& TIM_TimeBaseStructure);
	TIM_TimeBaseStructure.TIM_Prescaler = prescaler;
	TIM_TimeBaseStructure.TIM_Period = pwm_period - 1;
	TIM_TimeBaseStructure.TIM_ClockDivision = 0;
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up ;
	TIM_TimeBaseInit(TIM1, &TIM_TimeBaseStructure);


// Initialise the timer channels
	TIM_OCInitTypeDef  TIM_OCInitStructure;

	TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1;
	TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;

	TIM_OCInitStructure.TIM_Pulse = 0; //pwm to 0 on init
	TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High; // Pulse polarity
	//	  TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_Low;
	TIM_OCInitStructure.TIM_OCIdleState = TIM_OCIdleState_Set;

	// These settings must be applied on the timer 1.
	TIM_OCInitStructure.TIM_OutputNState = TIM_OutputNState_Disable;
	TIM_OCInitStructure.TIM_OCNPolarity = TIM_OCNPolarity_High;
	TIM_OCInitStructure.TIM_OCNIdleState = TIM_OCIdleState_Set;

// Setup four channels
	// Channel 1
	TIM_OC1Init(TIM1, &TIM_OCInitStructure);
	TIM_OC1PreloadConfig(TIM1, TIM_OCPreload_Enable);


	// Startup the timer
	//TIM_ARRPreloadConfig(TIM1, DISABLE);
	TIM_ARRPreloadConfig(TIM1, DISABLE);
	TIM_CtrlPWMOutputs(TIM1, ENABLE);
	TIM_Cmd(TIM1 , ENABLE);

	RCC->CFGR3 |= RCC_CFGR3_TIM1SW;
	return pwm_period;
}

void inline user_systick_handler_ms()
{
	ms++;
	if(ms-last_button>300 && button_cnt!=0)//300ms
	{
		switch(button_cnt){
		case 1: ss_1();break;
		case 2: ss_2();break;
		default: break;
		}
		button_cnt=0;
	}


}
__IO uint32_t TimingDelay = 0;


/**
 **===========================================================================
 **
 **  Abstract: SysTick interrupt handler
 **
 **===========================================================================
 */
void SysTick_Handler(void)
{
	if (TimingDelay != 0x00)
	{
		TimingDelay--;
	}

	user_systick_handler_ms();

}

/**
 * @brief  Inserts a delay time.
 * @param  ms: specifies the delay time length, in ms.
 * @retval None
 */
void _delay_ms(__IO uint32_t _ms)
{
	TimingDelay = _ms ;
	while(TimingDelay != 0);
}

/**
 * @brief  Inserts a delay time.
 * @param  ms: specifies the delay time length, in ms.
 * @retval None
 */
void _delay_us(__IO uint32_t _us)
{
	TimingDelay = _us/1024;//shouldnt use this function anyhoo
	while(TimingDelay != 0);
}


void EXTI0_Config(void)
{
  EXTI_InitTypeDef   EXTI_InitStructure;
  GPIO_InitTypeDef   GPIO_InitStructure;
  NVIC_InitTypeDef   NVIC_InitStructure;

  /* Enable GPIOA clock */
  RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);

  /* Configure PA0 pin as input floating */
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0;
  GPIO_Init(GPIOA, &GPIO_InitStructure);

  /* Enable SYSCFG clock */
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);

  /* Connect EXTI0 Line to PA0 pin */
  SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOA, EXTI_PinSource0);

  /* Configure EXTI0 line */
  EXTI_InitStructure.EXTI_Line = EXTI_Line0;
  EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
  EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising;
  EXTI_InitStructure.EXTI_LineCmd = ENABLE;
  EXTI_Init(&EXTI_InitStructure);

  /* Enable and set EXTI0 Interrupt to the lowest priority */
  NVIC_InitStructure.NVIC_IRQChannel = EXTI0_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x0F;
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x0F;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);
}

int main(void)
{	/* Example use SysTick timer and read System core clock */

	button_cnt=0;
	running=0;
	ms=0;
	SysTick_Config(72000);  /* 1 ms if clock frequency 72 MHz */

	SystemCoreClockUpdate();
	USART2_Init(115200);
	/* Set the SysTick Interrupt to occur every 1ms) */

	//if (SysTick_Config(RCC_Clocks.HCLK_Frequency / 1000))
		//while(1);																										//will end up in this infinite loop if there was an error with Systick_Config

	//HD44780_Init();												//initialize the lcd
	EXTI0_Config();
	//while(1);
	/*for(int i=0;i<4;i++)
	{
		HD44780_GotoXY(0,i);
		HD44780_PutStr("Hello World! ");				//print text
		HD44780_PutChar(i+'0');
	}*/


	/* Main program loop */

	init_pwm_pinmux();
	init_pwm(100000);
	RCC_ClocksTypeDef 			RCC_Clocks;															//structure used for setting up the SysTick Interrupt

	RCC_GetClocksFreq(&RCC_Clocks);
    /* TIM1CLK clock frequency */

    printf("%lu\n",RCC_Clocks.TIM1CLK_Frequency);

	set_duty_cycle(.1);
   while (1)
  {
	   printf("%lu\n",RCC_Clocks.TIM1CLK_Frequency);

	   for(float i=0;i<=1;i+=.01)
	   {
		   _delay_ms(10);
		   set_duty_cycle(i);
	   }
	   for(float i=1;i>=0;i-=.01)
	   	   {
	   		   _delay_ms(10);
	   		   set_duty_cycle(i);
	   	   }
  }





	/* Program will never run to this line */
	return 0;
}
