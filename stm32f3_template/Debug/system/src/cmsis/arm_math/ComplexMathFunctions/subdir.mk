################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../system/src/cmsis/arm_math/ComplexMathFunctions/arm_cmplx_conj_f32.c \
../system/src/cmsis/arm_math/ComplexMathFunctions/arm_cmplx_conj_q15.c \
../system/src/cmsis/arm_math/ComplexMathFunctions/arm_cmplx_conj_q31.c \
../system/src/cmsis/arm_math/ComplexMathFunctions/arm_cmplx_dot_prod_f32.c \
../system/src/cmsis/arm_math/ComplexMathFunctions/arm_cmplx_dot_prod_q15.c \
../system/src/cmsis/arm_math/ComplexMathFunctions/arm_cmplx_dot_prod_q31.c \
../system/src/cmsis/arm_math/ComplexMathFunctions/arm_cmplx_mag_f32.c \
../system/src/cmsis/arm_math/ComplexMathFunctions/arm_cmplx_mag_q15.c \
../system/src/cmsis/arm_math/ComplexMathFunctions/arm_cmplx_mag_q31.c \
../system/src/cmsis/arm_math/ComplexMathFunctions/arm_cmplx_mag_squared_f32.c \
../system/src/cmsis/arm_math/ComplexMathFunctions/arm_cmplx_mag_squared_q15.c \
../system/src/cmsis/arm_math/ComplexMathFunctions/arm_cmplx_mag_squared_q31.c \
../system/src/cmsis/arm_math/ComplexMathFunctions/arm_cmplx_mult_cmplx_f32.c \
../system/src/cmsis/arm_math/ComplexMathFunctions/arm_cmplx_mult_cmplx_q15.c \
../system/src/cmsis/arm_math/ComplexMathFunctions/arm_cmplx_mult_cmplx_q31.c \
../system/src/cmsis/arm_math/ComplexMathFunctions/arm_cmplx_mult_real_f32.c \
../system/src/cmsis/arm_math/ComplexMathFunctions/arm_cmplx_mult_real_q15.c \
../system/src/cmsis/arm_math/ComplexMathFunctions/arm_cmplx_mult_real_q31.c 

OBJS += \
./system/src/cmsis/arm_math/ComplexMathFunctions/arm_cmplx_conj_f32.o \
./system/src/cmsis/arm_math/ComplexMathFunctions/arm_cmplx_conj_q15.o \
./system/src/cmsis/arm_math/ComplexMathFunctions/arm_cmplx_conj_q31.o \
./system/src/cmsis/arm_math/ComplexMathFunctions/arm_cmplx_dot_prod_f32.o \
./system/src/cmsis/arm_math/ComplexMathFunctions/arm_cmplx_dot_prod_q15.o \
./system/src/cmsis/arm_math/ComplexMathFunctions/arm_cmplx_dot_prod_q31.o \
./system/src/cmsis/arm_math/ComplexMathFunctions/arm_cmplx_mag_f32.o \
./system/src/cmsis/arm_math/ComplexMathFunctions/arm_cmplx_mag_q15.o \
./system/src/cmsis/arm_math/ComplexMathFunctions/arm_cmplx_mag_q31.o \
./system/src/cmsis/arm_math/ComplexMathFunctions/arm_cmplx_mag_squared_f32.o \
./system/src/cmsis/arm_math/ComplexMathFunctions/arm_cmplx_mag_squared_q15.o \
./system/src/cmsis/arm_math/ComplexMathFunctions/arm_cmplx_mag_squared_q31.o \
./system/src/cmsis/arm_math/ComplexMathFunctions/arm_cmplx_mult_cmplx_f32.o \
./system/src/cmsis/arm_math/ComplexMathFunctions/arm_cmplx_mult_cmplx_q15.o \
./system/src/cmsis/arm_math/ComplexMathFunctions/arm_cmplx_mult_cmplx_q31.o \
./system/src/cmsis/arm_math/ComplexMathFunctions/arm_cmplx_mult_real_f32.o \
./system/src/cmsis/arm_math/ComplexMathFunctions/arm_cmplx_mult_real_q15.o \
./system/src/cmsis/arm_math/ComplexMathFunctions/arm_cmplx_mult_real_q31.o 

C_DEPS += \
./system/src/cmsis/arm_math/ComplexMathFunctions/arm_cmplx_conj_f32.d \
./system/src/cmsis/arm_math/ComplexMathFunctions/arm_cmplx_conj_q15.d \
./system/src/cmsis/arm_math/ComplexMathFunctions/arm_cmplx_conj_q31.d \
./system/src/cmsis/arm_math/ComplexMathFunctions/arm_cmplx_dot_prod_f32.d \
./system/src/cmsis/arm_math/ComplexMathFunctions/arm_cmplx_dot_prod_q15.d \
./system/src/cmsis/arm_math/ComplexMathFunctions/arm_cmplx_dot_prod_q31.d \
./system/src/cmsis/arm_math/ComplexMathFunctions/arm_cmplx_mag_f32.d \
./system/src/cmsis/arm_math/ComplexMathFunctions/arm_cmplx_mag_q15.d \
./system/src/cmsis/arm_math/ComplexMathFunctions/arm_cmplx_mag_q31.d \
./system/src/cmsis/arm_math/ComplexMathFunctions/arm_cmplx_mag_squared_f32.d \
./system/src/cmsis/arm_math/ComplexMathFunctions/arm_cmplx_mag_squared_q15.d \
./system/src/cmsis/arm_math/ComplexMathFunctions/arm_cmplx_mag_squared_q31.d \
./system/src/cmsis/arm_math/ComplexMathFunctions/arm_cmplx_mult_cmplx_f32.d \
./system/src/cmsis/arm_math/ComplexMathFunctions/arm_cmplx_mult_cmplx_q15.d \
./system/src/cmsis/arm_math/ComplexMathFunctions/arm_cmplx_mult_cmplx_q31.d \
./system/src/cmsis/arm_math/ComplexMathFunctions/arm_cmplx_mult_real_f32.d \
./system/src/cmsis/arm_math/ComplexMathFunctions/arm_cmplx_mult_real_q15.d \
./system/src/cmsis/arm_math/ComplexMathFunctions/arm_cmplx_mult_real_q31.d 


# Each subdirectory must supply rules for building sources it contributes
system/src/cmsis/arm_math/ComplexMathFunctions/%.o: ../system/src/cmsis/arm_math/ComplexMathFunctions/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -ffreestanding -Wunused -Wuninitialized -Wall -Wextra -Wmissing-declarations -Wconversion -Wpointer-arith -Wpadded -Wshadow -Wlogical-op -Waggregate-return -Wfloat-equal  -g3 -DDEBUG -DUSE_FULL_ASSERT -DTRACE -DOS_USE_TRACE_ITM -DSTM32F30X -DUSE_STDPERIPH_DRIVER -DHSE_VALUE=8000000 -D__FPU_PRESENT -I"../include" -I"../system/include" -I"../system/include/cmsis" -I"../system/include/stm32f3-stdperiph" -I../system/include/stm32f3-usb -I../bsp/inc -I../ee152_lib/inc -std=gnu11 -Wmissing-prototypes -Wstrict-prototypes -Wbad-function-cast -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


