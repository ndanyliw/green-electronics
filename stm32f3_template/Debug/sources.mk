################################################################################
# Automatically-generated file. Do not edit!
################################################################################

ELF_SRCS := 
O_SRCS := 
CPP_SRCS := 
C_UPPER_SRCS := 
C_SRCS := 
S_UPPER_SRCS := 
OBJ_SRCS := 
ASM_SRCS := 
CXX_SRCS := 
C++_SRCS := 
CC_SRCS := 
SECONDARY_SIZE := 
C++_DEPS := 
OBJS := 
C_DEPS := 
ASM_DEPS := 
CC_DEPS := 
SECONDARY_FLASH := 
CPP_DEPS := 
CXX_DEPS := 
C_UPPER_DEPS := 
S_UPPER_DEPS := 

# Every subdirectory with source files must be described here
SUBDIRS := \
system/src/stm32f3-usb \
system/src/stm32f3-stdperiph \
system/src/newlib \
system/src/diag \
system/src/cortexm \
system/src/cmsis \
system/src/cmsis/arm_math/TransformFunctions \
system/src/cmsis/arm_math/SupportFunctions \
system/src/cmsis/arm_math/StatisticsFunctions \
system/src/cmsis/arm_math/MatrixFunctions \
system/src/cmsis/arm_math/FilteringFunctions \
system/src/cmsis/arm_math/FastMathFunctions \
system/src/cmsis/arm_math/ControllerFunctions \
system/src/cmsis/arm_math/ComplexMathFunctions \
system/src/cmsis/arm_math/CommonTables \
system/src/cmsis/arm_math/BasicMathFunctions \
src \
ee152_lib/src \
bsp/src \

