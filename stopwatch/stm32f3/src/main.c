/**
 *****************************************************************************
 **
 **  File        : main.c
 **
 **  Abstract    : main function.
 **
 **  Functions   : main
 **
 **  Environment : Atollic TrueSTUDIO(R)
 **
 **  Distribution: The file is distributed �as is,� without any warranty
 **                of any kind.
 **
 **  (c)Copyright Atollic AB.
 **  You may use this file as-is or modify it according to the needs of your
 **  project. Distribution of this file (unmodified or modified) is not
 **  permitted. Atollic AB permit registered Atollic TrueSTUDIO(R) users the
 **  rights to distribute the assembled, compiled & linked contents of this
 **  file as part of an application binary file, provided that it is built
 **  using the Atollic TrueSTUDIO(R) toolchain.
 **
 *****************************************************************************
 */

/* Includes */
#include "stm32f30x.h"
#include "stm32f3_discovery.h"
#include "common.h"
#include "stm32f30x_rcc.h"
#include "HD44780_F3.h"

/* Private typedef */

/* Private define  */
#define DELAY	(1000)

/* Private macro */

/* Private variables */
float a, b, c, d;
int ms, running, button_cnt, mshl, stopwatch_min, stopwatch_ms, last_button;
volatile float pwm_value;
#define PWM_DELTA (.02)
#define MAX_PWM 1
void EXTI0_IRQHandler(void)
{
  //if ((EXTI_GetITStatus(USER_BUTTON_EXTI_LINE) == SET)&&(STM_EVAL_PBGetState(BUTTON_USER) != RESET))
  {
    unsigned int time=ms;
    unsigned int lb=last_button+1;
	if(time-last_button>50)//50ms
	{
		//is a valid press
		last_button=time;
		button_cnt++;
	}

    /* Clear the EXTI line 0 pending bit */

  }
  EXTI_ClearITPendingBit(USER_BUTTON_EXTI_LINE);
}
void set_duty_cycle(float duty)
{
	TIM_SetCompare1(TIM1 , (TIM1->ARR)*duty);
}
void ss_1()
{
	pwm_value+=PWM_DELTA;
	if(pwm_value>MAX_PWM)
	{
		pwm_value=MAX_PWM;
	}
	set_duty_cycle(pwm_value);

}
void ss_2()
{
	 pwm_value-=PWM_DELTA;
	if(pwm_value<0)
	{
		pwm_value=0;
	}
	set_duty_cycle(pwm_value);
}
void init_pwm_pinmux()
{
	GPIO_InitTypeDef GPIO_InitStructure;
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOE, ENABLE);
	GPIO_InitStructure.GPIO_Pin = LED3_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF; // Use the alternative pin functions
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz; // GPIO speed - has nothing to do with the timer timing
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP; // Push-pull
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP; // Setup pull-up resistors
	GPIO_Init(GPIOE, &GPIO_InitStructure);
	GPIO_PinAFConfig(GPIOE, GPIO_PinSource9, GPIO_AF_2); // TIM1_CH1 -> LED3
}

int init_pwm(int pwm_freq)
{
	// Calculates the timing. This is common for all channels
	int clk = 72e6; // 72MHz -> system core clock. This is default on the stm32f3 discovery
	int tim_freq = 72e6; // in Hz (2MHz) Base frequency of the pwm timer
	int prescaler = 1;

	// Calculate the period for a given pwm frequency
	int pwm_period = tim_freq/pwm_freq;

//  Enable the TIM1 peripherie
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM1 , ENABLE );

// Setup the timing and configure the TIM1 timer
	TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
	TIM_TimeBaseStructInit(& TIM_TimeBaseStructure);
	TIM_TimeBaseStructure.TIM_Prescaler = prescaler;
	TIM_TimeBaseStructure.TIM_Period = pwm_period - 1;
	TIM_TimeBaseStructure.TIM_ClockDivision = 0;
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up ;
	TIM_TimeBaseInit(TIM1, &TIM_TimeBaseStructure);


// Initialise the timer channels
	TIM_OCInitTypeDef  TIM_OCInitStructure;

	TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1;
	TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;

	TIM_OCInitStructure.TIM_Pulse = 0; //pwm to 0 on init
	TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High; // Pulse polarity
	//	  TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_Low;
	TIM_OCInitStructure.TIM_OCIdleState = TIM_OCIdleState_Set;

	// These settings must be applied on the timer 1.
	TIM_OCInitStructure.TIM_OutputNState = TIM_OutputNState_Disable;
	TIM_OCInitStructure.TIM_OCNPolarity = TIM_OCNPolarity_High;
	TIM_OCInitStructure.TIM_OCNIdleState = TIM_OCIdleState_Set;

// Setup four channels
	// Channel 1
	TIM_OC1Init(TIM1, &TIM_OCInitStructure);
	TIM_OC1PreloadConfig(TIM1, TIM_OCPreload_Enable);


	// Startup the timer
	//TIM_ARRPreloadConfig(TIM1, DISABLE);
	TIM_ARRPreloadConfig(TIM1, DISABLE);
	TIM_CtrlPWMOutputs(TIM1, ENABLE);
	TIM_Cmd(TIM1 , ENABLE);

	// The PWM is running now. The pulse width can be set by
	// TIM1->CCR1 = [0..pwm_period] -> 0..100% duty cycle
	//
	// For example:
	// int pulse_width = 3000;
	// TIM1->CCR1 = pulse_width;
	//
	// The firmware offers a API to do this:
	// TIM_SetCompare1(TIM1 , pulse_width); // This is a wrapper for TIM1->CCR1, the same as TIM1->CCR1=pulse_width;

	return pwm_period;
}

void inline user_systick_handler_ms()
{
	ms++;
	if(ms-last_button>300 && button_cnt!=0)//300ms
	{
		switch(button_cnt){
		case 1: ss_1();break;
		case 2: ss_2();break;
		default: break;
		}
		button_cnt=0;
	}
	if(!running)
	{
		return;
	}
	stopwatch_ms++;
	if(stopwatch_ms==60000)
	{
		stopwatch_ms=0;
		stopwatch_min++;
	}

}
__IO uint32_t TimingDelay = 0;


/**
 **===========================================================================
 **
 **  Abstract: SysTick interrupt handler
 **
 **===========================================================================
 */
void SysTick_Handler(void)
{
	if (TimingDelay != 0x00)
	{
		TimingDelay--;
	}

	user_systick_handler_ms();

}

/**
 * @brief  Inserts a delay time.
 * @param  ms: specifies the delay time length, in ms.
 * @retval None
 */
void _delay_ms(__IO uint32_t _ms)
{
	TimingDelay = _ms ;
	while(TimingDelay != 0);
}

/**
 * @brief  Inserts a delay time.
 * @param  ms: specifies the delay time length, in ms.
 * @retval None
 */
void _delay_us(__IO uint32_t _us)
{
	TimingDelay = _us/1024;//shouldnt use this function anyhoo
	while(TimingDelay != 0);
}
void inline toString(int num, int length, char* out)
{
	for(int i=length-1;i>=0;i--)
	{
		out[i]=num%10+'0';
		num/=10;
	}
}
void draw_display()
{
	HD44780_GotoXY(0,0);
	char min_buf[4];
	min_buf[3]=0;
	toString(stopwatch_min,3,min_buf);
	HD44780_PutStr(min_buf);
	HD44780_PutChar(':');
	char sec_buf[7];
	toString(stopwatch_ms,5,sec_buf);
	sec_buf[6]=0;
	sec_buf[5]=sec_buf[4];
	sec_buf[4]=sec_buf[3];
	sec_buf[3]=sec_buf[2];
	sec_buf[2]=':';
	HD44780_PutStr(sec_buf);
}

void EXTI0_Config(void)
{
  EXTI_InitTypeDef   EXTI_InitStructure;
  GPIO_InitTypeDef   GPIO_InitStructure;
  NVIC_InitTypeDef   NVIC_InitStructure;

  /* Enable GPIOA clock */
  RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);

  /* Configure PA0 pin as input floating */
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0;
  GPIO_Init(GPIOA, &GPIO_InitStructure);

  /* Enable SYSCFG clock */
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);

  /* Connect EXTI0 Line to PA0 pin */
  SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOA, EXTI_PinSource0);

  /* Configure EXTI0 line */
  EXTI_InitStructure.EXTI_Line = EXTI_Line0;
  EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
  EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising;
  EXTI_InitStructure.EXTI_LineCmd = ENABLE;
  EXTI_Init(&EXTI_InitStructure);

  /* Enable and set EXTI0 Interrupt to the lowest priority */
  NVIC_InitStructure.NVIC_IRQChannel = EXTI0_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x0F;
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x0F;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);
}

int main(void)
{	/* Example use SysTick timer and read System core clock */
	stopwatch_ms=0;
	stopwatch_min=0;
	mshl=0;
	button_cnt=0;
	running=0;
	ms=0;
	SysTick_Config(72000);  /* 1 ms if clock frequency 72 MHz */

	SystemCoreClockUpdate();
	volatile uint32_t ii;

	ii = SystemCoreClock;   /* This is a way to read the System core clock */
	/* Set the SysTick Interrupt to occur every 1ms) */
	//RCC_GetClocksFreq(&RCC_Clocks);
	//if (SysTick_Config(RCC_Clocks.HCLK_Frequency / 1000))
		//while(1);																										//will end up in this infinite loop if there was an error with Systick_Config

	HD44780_Init();												//initialize the lcd
	EXTI0_Config();
	//while(1);
	/*for(int i=0;i<4;i++)
	{
		HD44780_GotoXY(0,i);
		HD44780_PutStr("Hello World! ");				//print text
		HD44780_PutChar(i+'0');
	}*/


	/* Main program loop */
  /*

   while (1)
  {
		//do nothing
  }

*/


	/* Initialize LEDs and User Button available on STM32F3-Discovery board */
	STM_EVAL_LEDInit(LED3);
	STM_EVAL_LEDInit(LED4);
	STM_EVAL_LEDInit(LED5);
	STM_EVAL_LEDInit(LED6);
	STM_EVAL_LEDInit(LED7);
	STM_EVAL_LEDInit(LED8);
	STM_EVAL_LEDInit(LED9);
	STM_EVAL_LEDInit(LED10);

	//STM_EVAL_PBInit(BUTTON_USER, BUTTON_MODE_EXTI);

	while (1)
	{
		/* Waiting User Button is pressed */
		//while (UserButtonPressed == 0x00)
		{
			/* Toggle LD3 */
			STM_EVAL_LEDToggle(LED3);
			draw_display();

			/* Toggle LD5 */
			STM_EVAL_LEDToggle(LED5);
			draw_display();

			/* Toggle LD7 */
			STM_EVAL_LEDToggle(LED7);
			draw_display();

			/* Toggle LD9 */
			STM_EVAL_LEDToggle(LED9);
			draw_display();

			/* Toggle LD10 */
			STM_EVAL_LEDToggle(LED10);
			draw_display();

			/* Toggle LD8 */
			STM_EVAL_LEDToggle(LED8);
			draw_display();

			/* Toggle LD6 */
			STM_EVAL_LEDToggle(LED6);
			draw_display();

			/* Toggle LD4 */
			STM_EVAL_LEDToggle(LED4);
			draw_display();
		}
	}

	/* Program will never run to this line */
	return 0;
}
