/******************** (C) COPYRIGHT 2014 STMicroelectronics ********************
* File Name          : readme.txt
* Author             : STMicroelectronics
* Date               : March/2014
* Description        : This file describes how to add STM32F302R8 device
                       support for ARM-MDK v4.xx and ARM-MDK v5.xx
********************************************************************************

This package contains the needed files to be installed in order to support
STM32F302R8 device by ARM-MDK v4.xx/v5.xx.

Running the "MDK-ARM STM32F302R8_Support_V1.0.exe", will adds the following:

- STM32F302R8 Devices Database to the original Generic CPU Database included with Keil uVision4/uvision5.

- Automatic STM32F302R8 devices flash algorithm selection

Note:
=====
* Before installing the files mentioned above, you need to have ARM-MDK v4.xx 
or later installed. 
You can downlaod ARM-MDK from keil web site @ www.keil.com

* While running "MDK-ARM STM32F302R8_Support_V1.0", the MDK-ARM
install directory is set by default to "C:\keil", please change it manually if you have 
MDK-ARM installed at different location.

* To be able to choose your STM32F302R8 device: 
1. Open your project
2. Go to Project>"options for Target....""
3. The "Options for Target...."window appears
4. Click on Device Tab
5. In the Database dialog you should use the drop down box and select 
"STMicroelectronics STM32F302R8 Devices" 


******************* (C) COPYRIGHT 2014 STMicroelectronics *****END OF FILE******
